//
//  TitleCoordinator.swift
//  KanjiList
//
//  Created by Vadim Temnogrudov on 12/07/2018.
//  Copyright © 2018 Kolos. All rights reserved.
//

import UIKit

class TitleCoordinator: NavigationCoordinator {
    var nextTitleCoordinator: TitleCoordinator? = nil
    let nextPresenter: UINavigationController
    let shouldPush: Bool
    let index: Int
    init(presenter:UINavigationController, shouldPush:Bool = false, index: Int = 0) {
        self.shouldPush = shouldPush
        self.index = index
        if shouldPush {
            nextPresenter = presenter
        } else {
            nextPresenter = UINavigationController()
            nextPresenter.delegate = AppDelegate.instance().applicationCoordinator
        }
        super.init(presenter: presenter)
    }
    override func start() {
        
        let titleViewController = TitleInfoViewController(nibName: nil, bundle: nil)
        titleViewController.index = index
        titleViewController.router = self
        if shouldPush {
            nextPresenter.pushViewController(titleViewController, animated: true)
        } else {
            nextPresenter.setViewControllers([titleViewController], animated: false)
            nextPresenter.modalPresentationStyle = .overCurrentContext
            presenter.present(nextPresenter, animated: true, completion: nil)
        }
    }
}

extension TitleCoordinator: TitleInfoViewControllerRouter {
    func showNextInfo(viewController: TitleInfoViewController) {
        nextTitleCoordinator = TitleCoordinator(presenter: nextPresenter, shouldPush: true, index: viewController.index + 1)
        nextTitleCoordinator?.start()
    }
    
    func close(viewController: UIViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
}
