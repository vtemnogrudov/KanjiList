//
//  TitleInfoViewController.swift
//  KanjiList
//
//  Created by Vadim Temnogrudov on 12/07/2018.
//  Copyright © 2018 Kolos. All rights reserved.
//

import UIKit

protocol TitleInfoViewControllerRouter: class {
    func showNextInfo(viewController: TitleInfoViewController)
    func close(viewController: UIViewController)
}

class TitleInfoViewController: UIViewController {

    var index: Int = 0
    weak var router: TitleInfoViewControllerRouter?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if index == 0 {
            title = "Info"
        } else {
            title = "Info\(index)"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func closeTapped(_ sender: Any) {
        router?.close(viewController: self)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        router?.showNextInfo(viewController: self)
    }
}

extension TitleInfoViewController: AdViewInformer {
    func shouldShowAd() -> Bool {
        return true
    }
}
