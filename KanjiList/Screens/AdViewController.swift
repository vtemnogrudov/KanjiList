//
//  AdViewController.swift
//  KanjiList
//
//  Created by Vadim Temnogrudov on 12/07/2018.
//  Copyright © 2018 Kolos. All rights reserved.
//

import UIKit

protocol AdViewInformer {
    func shouldShowAd() -> Bool
}

class AdViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

     @IBOutlet weak var adViewHeight: NSLayoutConstraint!
     @IBOutlet weak var adView: UIView!
    
    func setupContainer(containerViewController: UINavigationController) {

       containerViewController.view.translatesAutoresizingMaskIntoConstraints = false
        addChildViewController(containerViewController)
        view.addSubview(containerViewController.view)
        let topConstraint = NSLayoutConstraint(item: containerViewController.view, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0)
        let leadingConstraint = NSLayoutConstraint(item: containerViewController.view, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 0)
        let trailingConstraint = NSLayoutConstraint(item: containerViewController.view, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: containerViewController.view, attribute: .bottom, relatedBy: .equal, toItem: adView, attribute: .top, multiplier: 1.0, constant: 0)
        view.addConstraints([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
    }

    func adView(visible: Bool, animated: Bool) {
        let animationTime = animated ? 0.2 : 0.0
        adViewHeight.constant = visible ? 50 : 0
        UIView.animate(withDuration: animationTime) {
            self.adView.alpha = visible ? 1.0 : 0.0
            self.view.layoutIfNeeded()
        }
    }
}
