//
//  UIViewController+Swizzling.m
//  KanjiList
//
//  Created by Vadim Temnogrudov on 13/07/2018.
//  Copyright © 2018 Kolos. All rights reserved.
//

#import "UIViewController+Swizzling.h"
#import <objc/runtime.h>

static NSObject<PresentationObserver> *s_presentationObserver = nil;

@implementation UIViewController (Swizzling)



+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        [self swizzleOriginalSelector:@selector(presentViewController:animated:completion:) swizzleSelector:@selector(xxx_presentViewController:animated:completion:)];
        [self swizzleOriginalSelector:@selector(dismissViewControllerAnimated:completion:) swizzleSelector:@selector(xxx_dismissViewControllerAnimated:completion:)];
    });
}

+ (void)swizzleOriginalSelector:(SEL)originalSelector swizzleSelector:(SEL)swizzleSelector {
    Class class = [self class];
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzleSelector);
    
    BOOL didAddMethod =
    class_addMethod(class,
                    originalSelector,
                    method_getImplementation(swizzledMethod),
                    method_getTypeEncoding(swizzledMethod));
    
    if (didAddMethod) {
        class_replaceMethod(class,
                            swizzleSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}

+ (void)setPresentationObserver:(NSObject<PresentationObserver> *)presentationObserver {
    @synchronized(s_presentationObserver) {
        if (!s_presentationObserver) {
            s_presentationObserver = presentationObserver;
        }
    }
}

#pragma mark - Method Swizzling

- (void)xxx_presentViewController:(UIViewController *)viewControllerToPresent animated: (BOOL)flag completion:(void (^ __nullable)(void))completion{
    [s_presentationObserver willPresentViewController:viewControllerToPresent animated:flag];
    [self xxx_presentViewController:viewControllerToPresent animated:flag completion:completion];
}

- (void)xxx_dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    [s_presentationObserver willDismissViewController:self animated:flag];
    [self xxx_dismissViewControllerAnimated:flag completion:^{
        [s_presentationObserver dismissedViewControllerAnimated:flag];
        if (completion) {
            completion();
        }
    }];
}
                  
                  
@end
