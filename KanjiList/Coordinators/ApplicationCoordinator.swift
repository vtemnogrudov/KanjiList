/// Copyright (c) 2018 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit

class ApplicationCoordinator: NSObject, Coordinator {
  let kanjiStorage: KanjiStorage //  1
  let window: UIWindow  // 2
  let rootViewController: UINavigationController  // 3
  let adViewController: AdViewController
  let allKanjiListCoordinator: AllKanjiListCoordinator
 
  init(window: UIWindow) {
    
    self.window = window
    kanjiStorage = KanjiStorage()
    rootViewController = UINavigationController()
    rootViewController.navigationBar.prefersLargeTitles = true  // 4
    
    adViewController = AdViewController(nibName: nil, bundle: nil)
    adViewController.setupContainer(containerViewController: rootViewController)
    
    allKanjiListCoordinator = AllKanjiListCoordinator(presenter: rootViewController, kanjiStorage: kanjiStorage)
    
    super.init()
    rootViewController.delegate = self
  }
  
  func start() {  // 6
    UIViewController.setPresentationObserver(self)
    window.rootViewController = adViewController
    allKanjiListCoordinator.start()
    window.makeKeyAndVisible()
  }
    private let defaultAdViewShow = false
    private func shouldShowAdBaner(viewController: UIViewController) -> Bool {
        guard let navigationController = viewController as? UINavigationController else {
            guard let adViewInformer = viewController as? AdViewInformer else {
                return defaultAdViewShow
            }
            return adViewInformer.shouldShowAd()
        }
        guard let topViewController = navigationController.topViewController else {
            return defaultAdViewShow
        }
        return shouldShowAdBaner(viewController:topViewController)
    }
    
   private func updateAdViewVisibility(currentController: UIViewController, animated: Bool) {
        adViewController.adView(visible: shouldShowAdBaner(viewController: currentController), animated: animated)
    }
}


extension ApplicationCoordinator: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        updateAdViewVisibility(currentController: viewController, animated: animated)
    }
}

extension ApplicationCoordinator: PresentationObserver {
    func willDismiss(_ viewController: UIViewController!, animated: Bool) {
        guard let presentingViewController = viewController.presentingViewController else { return }
        updateAdViewVisibility(currentController: presentingViewController, animated: animated)
    }
    
    func willPresent(_ viewController: UIViewController!, animated: Bool) {
        updateAdViewVisibility(currentController: viewController, animated: animated)
    }
    
    func dismissedViewController(animated: Bool) {
        guard let visibleViewController = rootViewController.visibleViewController else { return }
        updateAdViewVisibility(currentController: visibleViewController, animated: animated)
    }
}
