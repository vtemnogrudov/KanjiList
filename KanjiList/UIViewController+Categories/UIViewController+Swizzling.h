//
//  UIViewController+Swizzling.h
//  KanjiList
//
//  Created by Vadim Temnogrudov on 13/07/2018.
//  Copyright © 2018 Kolos. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PresentationObserver<NSObject>

- (void)willPresentViewController:(UIViewController *)viewController animated:(BOOL)animated;
- (void)willDismissViewController:(UIViewController *)viewController animated:(BOOL)animated;
- (void)dismissedViewControllerAnimated:(BOOL)animated;

@end

@interface UIViewController (Swizzling)

+ (void) setPresentationObserver:(NSObject<PresentationObserver> *)presentationObserver;

@end
