//
//  NavigationCoordinator.swift
//  KanjiList
//
//  Created by Vadim Temnogrudov on 12/07/2018.
//  Copyright © 2018 Kolos. All rights reserved.
//

import UIKit

class NavigationCoordinator: Coordinator {
    func start() {
        fatalError("start is not implemented in your class")
    }
    

    let presenter: UINavigationController
    
     internal init(presenter: UINavigationController) {
        self.presenter = presenter
    }
}
